## Library yg digunakan -> [PHPOffice](https://github.com/PHPOffice/PHPWord)

Pastikan semua reqruitment phpoffice sdh terinstall, [cek disini](https://github.com/PHPOffice/PHPWord#requirements)

### Instalasi 
```
composer install
```

### Jalankan Service
```
php -S 127.0.0.1:8181
```

Merubah value yg ada pada file .docx dengan parameter ```${key}``` dengan value tertentu. Hasil dapat di lihat di folder output 