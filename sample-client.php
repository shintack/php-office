<?php

$curl = curl_init();

$template = "sample-template.docx"; // silahkan replace ini dengan path file template yg akan digunakan

curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://127.0.0.1:8181',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => array(
        'template' => new CURLFILE($template),
        'data' => '{"title":"ini title", "content":"ini content"}' // silahkan sesuaikan json string data yg akan di gunakan 
    ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
