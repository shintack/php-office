<?php
require __DIR__ . '/vendor/autoload.php';

$fileLocation = parseValueToDocx();

header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
header("Cache-Control: public");
header("Content-Transfer-Encoding: Binary");
header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Disposition: attachment; filename="myfile.docx"');
header("Content-Length:" . filesize($fileLocation));
readfile($fileLocation);
die();

function prepareConvert()
{
    if (!$_FILES || !$_FILES['template']) exit('Template .docx required');

    $uploaddir = 'template/';

    // check dir template exist s
    if (!file_exists($uploaddir)) mkdir($uploaddir);

    $uploadfile = $uploaddir . basename($_FILES['template']['name']);

    // save template to localy
    move_uploaded_file($_FILES['template']['tmp_name'], $uploadfile);

    return $uploadfile;
}

function parseValueToDocx()
{
    if (!$_POST || !$_POST['data']) exit('Data in json formated REQUIRED');

    $jsonData = $_POST['data'];

    $templatePath  = prepareConvert();
    $template = json_decode($jsonData);

    $outputPath = "output";
    $fileOutput = "template-" . date('YmdHis') . '.docx';
    $finalOutput = $outputPath . DIRECTORY_SEPARATOR . $fileOutput;

    if (!file_exists($outputPath)) mkdir($outputPath);

    $phpword = new \PhpOffice\PhpWord\TemplateProcessor($templatePath);

    foreach ($template as $key => $value) {
        $phpword->setValue($key, $value);
    }

    $phpword->saveAs($finalOutput);

    return $finalOutput;
}
